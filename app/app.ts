const nameElement = document.getElementById("name") as HTMLSpanElement;
const hungerElement = document.getElementById("hunger") as HTMLSpanElement;
const happinessElement = document.getElementById("happiness") as HTMLSpanElement;
const energyElement = document.getElementById("energy") as HTMLSpanElement;
const feedBtn = document.getElementById("feedBtn") as HTMLButtonElement;
const playBtn = document.getElementById("playBtn") as HTMLButtonElement;
const sleepBtn = document.getElementById("sleepBtn") as HTMLButtonElement;


let tamagotchiName: string = "Тамагочи";
let hunger: number = 100;
let happiness: number = 100;
let energy: number = 100;


const updateStatus = (): void => {
    nameElement.innerText = tamagotchiName;

    if (hunger < 0) hunger = 0;

    if (happiness < 0) happiness = 0;

    if (energy < 0) energy = 0;

    hungerElement.innerText = hunger === 0 ? "Голоден" : String(hunger);
    happinessElement.innerText = happiness === 0 ? "Не счастлив" : String(happiness);
    energyElement.innerText = energy === 0 ? "Истощен" : String(energy);
};


const feedTamagotchi = (): void => {
    hunger += 10;
    happiness += 5;
    updateStatus();
};


const playWithTamagotchi = (): void => {
    hunger -= 5;
    happiness += 15;
    energy -= 10;
    updateStatus();
};


const putTamagotchiToSleep = (): void => {
    energy += 20;
    hunger -= 5;
    updateStatus();
};


feedBtn.addEventListener("click", feedTamagotchi);
playBtn.addEventListener("click", playWithTamagotchi);
sleepBtn.addEventListener("click", putTamagotchiToSleep);

updateStatus();
